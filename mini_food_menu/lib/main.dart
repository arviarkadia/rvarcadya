import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mini_food_menu/constraint.dart';
import 'package:mini_food_menu/detail_screes.dart';
import 'package:mini_food_menu/widgets/category_title.dart';

import 'widgets/food_card.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Food App Menu',
      theme: ThemeData(
        fontFamily: "Poppins",
        scaffoldBackgroundColor: kWhiteColor,
        primaryColor: kPrimaryColor,
      ),
      home: HomeScreen(),
    );
  }
}

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // floating button
      floatingActionButton: Container(
        padding: EdgeInsets.all(10),
        height: 80,
        width: 80,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: kPrimaryColor.withOpacity(.26),
        ),
        child: Container(
          // height: 60,
          // width: 60,
          padding: EdgeInsets.all(20),
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: kPrimaryColor,
          ),
          child: SvgPicture.asset("assets/icons/plus.svg"),
        ),
      ),
      // end
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(
                top: 50,
                right: 20,
              ),
              child: Align(
                  alignment: Alignment.topRight,
                  child: SvgPicture.asset("assets/icons/menu.svg")),
            ),
            // title
            Padding(
              padding: const EdgeInsets.all(20),
              child: Text(
                "Simple way to find \n Tasty food",
                style: Theme.of(context).textTheme.headline5!.copyWith(
                      fontWeight: FontWeight.bold,
                    ),
              ),
            ),
            // categories
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: const [
                  CategoryTitle(title: 'All', active: true),
                  CategoryTitle(title: 'Italian', active: true),
                  CategoryTitle(title: 'English', active: true),
                  CategoryTitle(title: 'Chinese', active: true),
                  CategoryTitle(title: 'Bulgarian', active: true),
                  CategoryTitle(title: 'Indonesian', active: true),
                ],
              ),
            ),
            // new search bar
            Container(
              alignment: Alignment.centerLeft,
              margin: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 12),
              width: double.infinity,
              height: 50,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                border: Border.all(color: kBorderColor),
              ),
              child: SvgPicture.asset("assets/icons/search.svg"),
            ),
            // new card product
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: [
                  FoodCard(
                    calories: '436 KCal',
                    description:
                        'Selamat menikmati makanadan yang anda hidangkan selama ini berkash bagi kalian semua selamat mencoba hidup baru',
                    image: 'assets/images/image_1.png',
                    ingredient: 'With Red Tomato',
                    price: 21,
                    title: 'Vegan Salad Bowl',
                    press: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return const DetailScreen(
                          description:
                              'Selamat menikmati makanadan yang anda hidangkan selama ini berkash bagi kalian semua selamat mencoba hidup baru',
                          image: 'assets/images/image_1.png',
                          ingredient: 'With Red Tomato',
                          title: 'Vegan Salad Bowl',
                          price: 21,
                        );
                      }));
                    },
                  ),
                  FoodCard(
                    calories: '500 KCal',
                    description:
                        'Selamat menikmati makanadan yang anda hidangkan selama ini berkash bagi kalian semua selamat mencoba hidup baru',
                    image: 'assets/images/image_2.png',
                    ingredient: 'With Red Cabagge',
                    price: 19,
                    title: 'Salad Bowl',
                    press: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return const DetailScreen(
                          description:
                              'Selamat menikmati makanadan yang anda hidangkan selama ini berkash bagi kalian semua selamat mencoba hidup baru',
                          image: 'assets/images/image_2.png',
                          ingredient: 'With Red Cabagge',
                          price: 19,
                          title: 'Salad Bowl',
                        );
                      }));
                    },
                  ),
                  SizedBox(
                    width: 50,
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
