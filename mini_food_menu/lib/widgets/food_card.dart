import 'package:flutter/material.dart';
import 'package:mini_food_menu/constraint.dart';

class FoodCard extends StatelessWidget {
  final String title;
  final String ingredient;
  final String image;
  final int price;
  final String calories;
  final String description;
  final VoidCallback press;

  const FoodCard({
    super.key,
    required this.title,
    required this.ingredient,
    required this.image,
    required this.price,
    required this.calories,
    required this.description,
    required this.press,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: press,
      child: Container(
        height: 400,
        width: 270,
        margin: EdgeInsets.only(left: 20),
        child: Stack(
          children: [
            // box field
            Positioned(
              right: 0,
              bottom: 0,
              child: Container(
                height: 380,
                width: 250,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(34),
                    color: kPrimaryColor.withOpacity(.06)),
              ),
            ),
            // circle field - rounded background
            Positioned(
              top: 10,
              left: 10,
              child: Container(
                height: 181,
                width: 181,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: kPrimaryColor.withOpacity(.15),
                ),
              ),
            ),
            //  new food images
            Positioned(
              top: 0,
              left: -50,
              child: Container(
                height: 184,
                width: 276,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage(image),
                  ),
                ),
              ),
            ),
            // new price
            Positioned(
              right: 20,
              left: 190,
              child: Padding(
                padding: const EdgeInsets.only(top: 80, left: 10),
                child: Text(
                  "\$${price}",
                  style: Theme.of(context).textTheme.headline5!.copyWith(
                        color: kPrimaryColor,
                        fontWeight: FontWeight.bold,
                      ),
                ),
              ),
            ),
            // new description
            Positioned(
              top: 201,
              left: 40,
              child: Container(
                width: 210,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    // main
                    Text(
                      title,
                      style: Theme.of(context)
                          .textTheme
                          .titleMedium!
                          .copyWith(fontWeight: FontWeight.bold),
                    ),
                    // sub
                    Text(
                      ingredient,
                      style: TextStyle(
                        color: kTextColor.withOpacity(.4),
                      ),
                    ),
                    // desc
                    SizedBox(
                      height: 16,
                    ),
                    Text(
                      description,
                      maxLines: 4,
                      style: TextStyle(
                        color: kTextColor.withOpacity(.7),
                      ),
                    ),
                    // other
                    SizedBox(
                      height: 16,
                    ),
                    Text(
                      calories,
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
