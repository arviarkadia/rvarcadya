# mini_food_menu

A new Flutter project.

## Getting Started

**Fonts**

- Poppins [link](https://fonts.google.com/specimen/Poppins)

**UI Credit**

- Design by: Saini UIUX Designer [link](https://www.uplabs.com/posts/happy-meals-food-delivery-app)

We design two pages one is the items page and another one is details page that will help you to design clear interfaces for food delivery app faster and easier.

### Food App Final UI

![App UI](https://gitlab.com/arviarkadia/rvarcadya/-/blob/main/mini_food_menu/food_app.png)
