import 'package:ecommerceapp/constraints.dart';
import 'package:ecommerceapp/model/product.dart';
import 'package:ecommerceapp/screens/detail/component/color_dot.dart';
import 'package:flutter/material.dart';

class ColorAndSize extends StatelessWidget {
  const ColorAndSize({
    super.key,
    required this.product,
  });

  final Product product;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Expanded(
          child: Column(
            children: <Widget>[
              Text("Color"),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  DotColor(
                    color: Color(0xff356c95),
                    isSelected: true,
                  ),
                  DotColor(
                    color: Color(0xfff8c078),
                  ),
                  DotColor(
                    color: Color(0xfffa29b9b),
                  ),
                ],
              ),
            ],
          ),
        ),
        // new
        Expanded(
          child: RichText(
            text: TextSpan(
              style: TextStyle(color: kTextColor),
              children: [
                TextSpan(text: "Size\n"),
                TextSpan(
                  text: "${product.size} cm",
                  style: Theme.of(context).textTheme.headline5!.copyWith(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                      ),
                ),
              ],
            ),
          ),
        ),
        // end
      ],
    );
  }
}
