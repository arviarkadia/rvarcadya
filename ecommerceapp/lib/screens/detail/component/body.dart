import 'package:ecommerceapp/constraints.dart';
import 'package:ecommerceapp/model/product.dart';
import 'package:ecommerceapp/screens/detail/component/add_to_cart.dart';
import 'package:ecommerceapp/screens/detail/component/cart_counter.dart';
import 'package:ecommerceapp/screens/detail/component/color_and_size.dart';
import 'package:ecommerceapp/screens/detail/component/counter_and_favorite.dart';
import 'package:ecommerceapp/screens/detail/component/product_description.dart';
import 'package:ecommerceapp/screens/detail/component/product_title_with_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class BodyDetail extends StatelessWidget {
  const BodyDetail({super.key, required this.product});
  final Product product;
  @override
  Widget build(BuildContext context) {
    // provide total height and width
    Size size = MediaQuery.of(context).size;
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          SizedBox(
            height: size.height,
            child: Stack(
              children: <Widget>[
                // body container bottom
                Container(
                  margin: EdgeInsets.only(top: size.height * 0.3),
                  padding: EdgeInsets.only(
                    top: size.height * 0.12,
                    left: kDefaultPaddin,
                    right: kDefaultPaddin,
                  ),
                  // height: 500,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(24.0),
                      topRight: Radius.circular(24.0),
                    ),
                  ),
                  // new
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: kDefaultPaddin / 2),
                      ColorAndSize(product: product),
                      SizedBox(height: kDefaultPaddin / 2),
                      DescriptionProduct(product: product),
                      SizedBox(height: kDefaultPaddin / 2),
                      CounterWithFavoriteButton(),
                      SizedBox(height: kDefaultPaddin / 2),
                      AddToCart(product: product)
                    ],
                  ),
                  // end
                ),
                // end
                // title
                ProductTitleWithImage(product: product)
                // end
              ],
            ),
          )
        ],
      ),
    );
  }
}
