import 'package:ecommerceapp/constraints.dart';
import 'package:ecommerceapp/model/product.dart';
import 'package:flutter/material.dart';

class itemCard extends StatelessWidget {
  final Product product;
  final VoidCallback press;
  const itemCard({
    super.key,
    required this.product,
    required this.press,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: press,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          // icon of product dummy
          Expanded(
            child: Container(
              padding: EdgeInsets.all(kDefaultPaddin),
              // for demo using fixed
              // height: 180,
              // width: 180,
              decoration: BoxDecoration(
                color: product.color,
                borderRadius: BorderRadius.circular(16),
              ),
              child: Hero(
                tag: "${product.id}",
                child: Image.asset(product.image),
              ),
            ),
          ),
          // name of product dummy
          Padding(
            padding: const EdgeInsets.symmetric(vertical: kDefaultPaddin / 4),
            child: Text(
              product.title,
              style: TextStyle(
                color: kTextLightColor,
              ),
            ),
          ),
          // price
          Text(
            "\$${product.price}",
            style: TextStyle(fontWeight: FontWeight.bold),
          )
        ],
      ),
    );
  }
}
