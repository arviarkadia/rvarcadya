import 'package:ecommerceapp/constraints.dart';
import 'package:ecommerceapp/screens/component/body.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // App Bar
      appBar: buildAppBar(),
      // body
      body: Body(),
    );
  }

  AppBar buildAppBar() {
    return AppBar(
      backgroundColor: Colors.white,
      elevation: 0,
      // back
      leading: IconButton(
        icon: SvgPicture.asset("assets/icons/back.svg"),
        onPressed: () {},
      ),
      actions: <Widget>[
        // search
        IconButton(
          onPressed: () {},
          icon: SvgPicture.asset(
            "assets/icons/search.svg",
            // default icon color white, so using from constarint
            color: kTextColor,
          ),
        ),
        // cart
        IconButton(
          onPressed: () {},
          icon: SvgPicture.asset(
            "assets/icons/cart.svg",
            // default icon color white, so using from constarint
            color: kTextColor,
          ),
        ),
        // divider
        const SizedBox(width: kDefaultPaddin / 2)
      ],
    );
  }
}
