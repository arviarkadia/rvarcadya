import 'package:flutter/material.dart';
import 'package:mini_ebook_app/screens/home_screens.dart';
import 'package:mini_ebook_app/widgets/rounded_button.dart';

class WelcomeScreen extends StatelessWidget {
  const WelcomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        // image background decoration
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/Bitmap.png"),
            fit: BoxFit.fill,
          ),
        ),
        // texfield
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            RichText(
              // title
              text: TextSpan(
                style: Theme.of(context).textTheme.displaySmall,
                children: [
                  TextSpan(
                    text: "flamin",
                  ),
                  TextSpan(
                    text: "Go",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
            ),
            // button
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.6,
              child: RoundedButton(
                text: 'Start Reading',
                fontSize: 20,
                press: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return HomeScreens();
                  }));
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
