import 'package:flutter/material.dart';
import 'package:mini_ebook_app/consttants.dart';
import 'package:mini_ebook_app/screens/details_screen.dart';
import 'package:mini_ebook_app/screens/home_screens.dart';
import 'package:mini_ebook_app/screens/welcome_page.dart';

import 'widgets/rounded_button.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Ebook App',
      theme: ThemeData(
        scaffoldBackgroundColor: Colors.white,
        textTheme: Theme.of(context).textTheme.apply(
              displayColor: kBlackColor,
            ),
      ),
      home: WelcomeScreen(),
    );
  }
}
