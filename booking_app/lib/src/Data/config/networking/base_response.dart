import 'package:booking_app/src/Data/data/models/auth/login.dart';
import 'package:booking_app/src/Data/data/models/trip/trip.dart';

class BaseResponse {
  String? message;
  Result? result;

  BaseResponse({
    this.message,
    this.result,
  });

  BaseResponse.fromJson(dynamic json) {
    message = json['message'];
    result = json['result'] != null ? Result.fromJson(json['result']) : null;
  }
  BaseResponse copyWith({
    String? message,
    Result? result,
  }) =>
      BaseResponse(
        message: message ?? this.message,
        result: result ?? this.result,
      );
  // Map<String, dynamic> toJson() {
  //   final Map<String, dynamic> data = new Map<String, dynamic>();
  //   data['message'] = this.message;
  //   if (this.result != null) {
  //     data['result'] = this.result!.toJson();
  //   }
  //   return data;
  // }
}

class Result {
  Login? login;
  Trip? trip;
  List<Trip>? yourRoomies;
  List<Trip>? otherMatches;

  Result({
    this.login,
    this.trip,
    this.yourRoomies,
    this.otherMatches,
  });

  Result.fromJson(Map<String, dynamic> json) {
    login = json['login'] != null ? new Login.fromJson(json['login']) : null;
    trip = json['trip'] != null ? new Trip.fromJson(json['trip']) : null;
    if (json['yourRoomies'] != null) {
      yourRoomies = [];
      json['yourRoomies'].forEach((v) {
        yourRoomies?.add(Trip.fromJson(v));
      });
    }
    if (json['otherMatches'] != null) {
      otherMatches = [];
      json['otherMatches'].forEach((v) {
        otherMatches?.add(Trip.fromJson(v));
      });
    }
  }

  Result copyWith({
    Login? login,
    Trip? trip,
    List<Trip>? yourRoomies,
    List<Trip>? otherMatches,
  }) =>
      Result(
        login: login ?? this.login,
        trip: trip ?? this.trip,
        yourRoomies: yourRoomies ?? this.yourRoomies,
        otherMatches: otherMatches ?? this.otherMatches,
      );

  Map<String, dynamic> toJson() => {
        'login': login,
        'trip': trip?.toJson(),
        'yourRoomies': yourRoomies?.map((e) => e.toJson() ?? []),
        'otherMatches': otherMatches?.map((e) => e.toJson() ?? []),
      };
}

/*
class Login {
  String? token;

  Login({this.token});

  Login.fromJson(Map<String, dynamic> json) {
    token = json['token'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['token'] = this.token;
    return data;
  }
}
*/