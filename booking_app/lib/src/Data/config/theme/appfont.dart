// import 'package:flutter/cupertino.dart';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class AppFont {
  static TextStyle get hero => TextStyle(
        fontSize: 72.sp,
        fontWeight: FontWeight.w700,
        color: Colors.black,
      );
  static TextStyle get diplay1 => TextStyle(
        fontSize: 56.sp,
        fontWeight: FontWeight.w700,
        color: Colors.black,
      );
  static TextStyle get diplay2 => TextStyle(
        fontSize: 40.sp,
        fontWeight: FontWeight.w700,
        color: Colors.black,
      );
  static TextStyle get diplay3 => TextStyle(
        fontSize: 32.sp,
        fontWeight: FontWeight.w700,
        color: Colors.black,
      );
  static TextStyle get heading1 => TextStyle(
        fontSize: 24.sp,
        fontWeight: FontWeight.w700,
        color: Colors.black,
      );
  static TextStyle get heading2 => TextStyle(
        fontSize: 20.sp,
        fontWeight: FontWeight.w700,
        color: Colors.black,
      );
  static TextStyle get paragraphLarge => TextStyle(
        fontSize: 16.sp,
        fontWeight: FontWeight.w400,
        color: Colors.black,
      );
  static TextStyle get paragraphLargeBold => TextStyle(
        fontSize: 16.sp,
        fontWeight: FontWeight.w700,
        color: Colors.black,
      );
  static TextStyle get paragraphMedium => TextStyle(
        fontSize: 14.sp,
        fontWeight: FontWeight.w400,
        color: Colors.black,
      );
  static TextStyle get paragraphMediumBold => TextStyle(
        fontSize: 14.sp,
        fontWeight: FontWeight.w700,
        color: Colors.black,
      );
  static TextStyle get paragraphSmall => TextStyle(
        fontSize: 12.sp,
        fontWeight: FontWeight.w400,
        color: Colors.black,
      );
  static TextStyle get paragraphSmallBold => TextStyle(
        fontSize: 12.sp,
        fontWeight: FontWeight.w700,
        color: Colors.black,
      );
  static TextStyle get buttonLarge => TextStyle(
        fontSize: 16.sp,
        fontWeight: FontWeight.w600,
        color: Colors.black,
      );
  static TextStyle get buttonMedium => TextStyle(
        fontSize: 14.sp,
        fontWeight: FontWeight.w600,
        color: Colors.black,
      );
  static TextStyle get buttonSmall => TextStyle(
        fontSize: 12.sp,
        fontWeight: FontWeight.w600,
        color: Colors.black,
      );
  static TextStyle get commponentLarge => TextStyle(
        fontSize: 16.sp,
        fontWeight: FontWeight.w600,
        color: Colors.black,
      );
  static TextStyle get commponentMediumBold => TextStyle(
        fontSize: 14.sp,
        fontWeight: FontWeight.w600,
        color: Colors.black,
      );
  static TextStyle get commponentMedium => TextStyle(
        fontSize: 14.sp,
        fontWeight: FontWeight.w400,
        color: Colors.black,
      );
  static TextStyle get commponentSmall => TextStyle(
        fontSize: 12.sp,
        fontWeight: FontWeight.w600,
        color: Colors.black,
      );
  static TextStyle get h0 => TextStyle(
    fontSize: 72.sp,
    fontWeight: FontWeight.w700,
    color: Colors.black,
  );
  static TextStyle get h1 => TextStyle(
    fontSize: 56.sp,
    fontWeight: FontWeight.w700,
    color: Colors.black,
  );
  static TextStyle get h2 => TextStyle(
    fontSize: 40.sp,
    fontWeight: FontWeight.w700,
    color: Colors.black,
  );
  static TextStyle get h3 => TextStyle(
    fontSize: 32.sp,
    fontWeight: FontWeight.w700,
    color: Colors.black,
  );
  static TextStyle get h4 => TextStyle(
    fontSize: 24.sp,
    fontWeight: FontWeight.w700,
    color: Colors.black,
  );
  static TextStyle get h5 => TextStyle(
    fontSize: 20.sp,
    fontWeight: FontWeight.w700,
    color: Colors.black,
  );
}
