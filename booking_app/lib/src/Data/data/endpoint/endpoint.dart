class Endpoint {
  static const String baseURL =
      "https://43930851-c586-489a-8d60-49e5d5967078.mock.pstmn.io";
  static const String login = "$baseURL/api/v1/login";
  static const String register = "$baseURL/api/v1/register";
  static const String forgotPassword = "$baseURL/api/v1/forgotPassword";
  static const String logout = "$baseURL/api/v1/logout";
  static const String trips = "$baseURL/api/v1/trip";
}
