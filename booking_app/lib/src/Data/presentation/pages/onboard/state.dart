import 'package:booking_app/src/Data/data/models/onboarding/onboarding.dart';
import 'package:booking_app/src/Data/data/src/img_string.dart';

class OnboardState {
  final List<Onboarding> onboardList = [
    Onboarding(
      title: 'Find a Roomie!',
      description:
          'We\'ve helped millions across the nation find their perfect match... and you\'re next!',
      image: ImgString.standing,
    ),
    Onboarding(
      title: 'Your Roommate Finder',
      description:
          'Hey you, looking for a roommate? We\'ve helped millions across the nation find their perfect match... and you\'re next!',
      image: ImgString.standing1,
    ),
    Onboarding(
      title: 'Find Your Match!',
      description:
          'We\'ve helped millions across the nation find their perfect match... and you\'re next!',
      image: ImgString.standing2,
    ),
  ];
  int currentIndex = 0;
  OnboardState init() {
    return OnboardState();
  }

  OnboardState clone() {
    return OnboardState();
  }
}
