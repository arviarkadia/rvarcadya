import 'package:bloc/bloc.dart';
import 'package:booking_app/src/Data/presentation/pages/pages.dart';
import 'package:booking_app/src/Data/presentation/pages/welcome/login/login_view.dart';
import 'package:booking_app/src/Data/utils/helpers/pref_helper.dart';
import 'package:get/get.dart';
import 'state.dart';

class OnboardCubit extends Cubit<OnboardState> {
  OnboardCubit() : super(OnboardState().init());

  void next() {
    if (state.currentIndex < state.onboardList.length - 1) {
      emit(state.clone()..currentIndex = state.currentIndex + 1);
    } else if (state.currentIndex == state.onboardList.length - 1) {
      skip();
    }
  }

  void previous() {
    if (state.currentIndex > 0) {
      emit(state.clone()..currentIndex = state.currentIndex - 1);
    }
  }

  void swiping(int index) {
    if (index >= 0 && index < state.onboardList.length) {
      emit(state.clone()..currentIndex = index);
    }
  }

  void skip() {
    // TODO
    PrefHelper.instance.isFirstInsatll;
    Get.to(LoginPage());
  }
}
