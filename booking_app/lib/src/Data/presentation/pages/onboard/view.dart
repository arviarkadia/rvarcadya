import 'package:booking_app/src/Data/utils/extension/double_extension.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'cubit.dart';
// import 'state.dart';
import 'component/component.dart';

class OnboardPage extends StatelessWidget {
  const OnboardPage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) => OnboardCubit(),
      child: Builder(builder: (context) => _buildPage(context)),
    );
  }

  Widget _buildPage(BuildContext context) {
    // final cubit = BlocProvider.of<OnboardCubit>(context);

    return Scaffold(
      // body: SingleChildScrollView(
      //   child: Column(
      //     children: const [
      //       OnboardImage(),
      //       OnboardingTitle(),
      //       OnboardingIndicator(),
      //       OnboardingButton(),
      //   ],
      //   ),
      // )
      body: Padding(
        padding: const EdgeInsets.only(
          top: 25,
          left: 16,
          right: 16,
        ),
        child: Column(
          children: [
            const OnboardImage(),
            32.0.height,
            const OnboardingTitle(),
            45.0.height,
            const OnboardingIndicator(),
            53.0.height,
            const OnboardingButton(),
            82.9.height,
          ],
        ),
      ),
    );
  }
}
