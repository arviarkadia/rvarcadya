import 'package:booking_app/src/Data/config/config.dart';
import 'package:booking_app/src/Data/presentation/pages/onboard/state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../cubit.dart';

class OnboardingIndicator extends StatelessWidget {
  const OnboardingIndicator({super.key});

  @override
  Widget build(BuildContext context) {
    final cubit = BlocProvider.of<OnboardCubit>(context);
    return BlocBuilder<OnboardCubit, OnboardState>(
      builder: (context, state) {
        return Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: List.generate(
              cubit.state.onboardList.length,
              (index) => index == cubit.state.currentIndex
                  ? _activeIndicator(index)
                  : _inactiveIndicator(index),
            ));
      },
    );
  }

  Widget _activeIndicator(int index) {
    return Container(
      width: 16,
      height: 6,
      margin: EdgeInsets.symmetric(horizontal: index==1 ? 12 : 0),
      decoration: BoxDecoration(
        color: AppColor.accentPink,
        borderRadius: BorderRadius.circular(4),
      ),
    );
  }

  Widget _inactiveIndicator(int index) {
    return Container(
      width: 6,
      height: 6,
      margin: EdgeInsets.symmetric(horizontal: index==1 ? 12 : 0),
      decoration: BoxDecoration(
        color: AppColor.ink03,
        borderRadius: BorderRadius.circular(4),
      ),
    );
  }
}
