import 'package:flutter/material.dart';
// import 'package:booking/presentation/pages/onboarding/onboarding_cubit.dart';
// import 'package:flutter/material.dart';
// ignore: unused_import
import 'package:provider/provider.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../cubit.dart';
// import '../onboarding_cubit.dart';
// import 'package:booking_app/src/Data/presentation/pages/onboard/view.dart';
// import 'package:provider/provider.dart';

class OnboardImage extends StatelessWidget {
  const OnboardImage({super.key});

  @override
  Widget build(BuildContext context) {
    final cubit = BlocProvider.of<OnboardCubit>(context);
    return Expanded(
      child: PageView.builder(
        itemBuilder: (c, i) => Image.asset(
          cubit.state.onboardList[i].image ?? "",
        ),
        itemCount: cubit.state.onboardList.length,
        onPageChanged: cubit.swiping,
      ),
    );
  }
}
