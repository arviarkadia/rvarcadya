import 'package:booking_app/src/Data/presentation/widgets/primary_button.dart';
import 'package:flutter/material.dart';

import '../../../../../config/config.dart';

class WelcomeButton extends StatelessWidget {
  const WelcomeButton({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: AppDimension.w16),
      child: PrimaryButton(
        onPressed: () {},
        text: "Let's Go",
        width: double.infinity,
      ),
    );
  }
}
