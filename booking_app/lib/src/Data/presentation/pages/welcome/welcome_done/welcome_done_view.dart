import 'package:booking_app/src/Data/config/theme/theme.dart';
import 'package:booking_app/src/Data/presentation/pages/welcome/welcome_done/component/welcome_button.dart';
import 'package:booking_app/src/Data/presentation/pages/welcome/welcome_done/component/welcome_images.dart';
import 'package:booking_app/src/Data/presentation/pages/welcome/welcome_done/component/welcome_title.dart';
import 'package:booking_app/src/Data/utils/extension/double_extension.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'welcome_done_cubit.dart';
import 'welcome_done_state.dart';

class WelcomedonePage extends StatelessWidget {
  const WelcomedonePage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) => Welcome_doneCubit(),
      child: Builder(builder: (context) => _buildPage(context)),
    );
  }

  Widget _buildPage(BuildContext context) {
    final cubit = BlocProvider.of<Welcome_doneCubit>(context);

    return Scaffold(
      backgroundColor: AppColor.accentGreen,
      body: Column(
        children: [
          102.0.height,
          const WelcomeImage(),
          16.0.height,
          const WelcomeTitle(),
          24.0.height,
          const WelcomeButton(),
          95.0.height,
        ],
      ),


    );
  }
}


