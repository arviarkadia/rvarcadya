import 'package:booking_app/src/Data/data/src/img_string.dart';
import 'package:flutter/material.dart';

import '../../../../../config/config.dart';

class WelcomeImage extends StatelessWidget {
  const WelcomeImage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: Padding(
            padding: EdgeInsets.symmetric(horizontal: AppDimension.w16),
            child: Image.asset(ImgString.backdrop),
    ));
  }
}
