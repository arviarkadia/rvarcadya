import 'package:booking_app/src/Data/presentation/pages/welcome/login/component/login_footer.dart';
import 'package:booking_app/src/Data/presentation/pages/welcome/login/component/login_form.dart';
import 'package:booking_app/src/Data/presentation/pages/welcome/login/component/login_images.dart';
import 'package:booking_app/src/Data/utils/extension/double_extension.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'login_cubit.dart';
import 'login_state.dart';
import 'package:booking_app/src/Data/utils/extension/extension.dart';

class LoginPage extends StatelessWidget {
  LoginPage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) => LoginCubit(),
      child: Builder(builder: (context) => _buildPage(context)),
    );
  }

  Widget _buildPage(BuildContext context) {
    // final cubit = BlocProvider.of<LoginCubit>(context);
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: SingleChildScrollView(
        child: Column(
          children: [
            60.0.height,
            const LoginImages(),
            40.0.height,
            const LoginForm(),
            16.0.height,
            const LoginFooter(),
          ],
        ),
      ),
    );
  }
}
