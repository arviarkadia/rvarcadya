import 'package:booking_app/src/Data/data/src/img_string.dart';
import 'package:flutter/material.dart';

class LoginImages extends StatelessWidget {
  const LoginImages({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Image.asset(ImgString.plants2);

  }
}
