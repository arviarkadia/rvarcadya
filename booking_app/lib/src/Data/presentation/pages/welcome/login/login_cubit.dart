import 'package:bloc/bloc.dart';
import 'package:booking_app/src/Data/config/config.dart';
import 'package:booking_app/src/Data/domain/controller/auth_controller.dart';
import 'package:booking_app/src/Data/presentation/pages/discover/discover_view.dart';
import 'package:booking_app/src/Data/utils/helpers/pref_helper.dart';
import 'package:get/get.dart';
import 'package:get/utils.dart';
import 'package:logging/logging.dart';

import 'login_state.dart';

class LoginCubit extends Cubit<LoginState> implements HttpState {
  LoginCubit() : super(LoginState().init());
  late final AuthController _authController = AuthController(this);

  void login() async {
    BaseResponse baseResponse = await _authController.login(
        state.emailController.text, state.passwordController.text);
    PrefHelper.instance.saveToken(baseResponse.result?.login?.token ?? "");
    Logger.root.info("TOKEN ${PrefHelper.instance.token}");
    Get.to(DiscoverPage());
  }

  @override
  void onEndRequest(String url, String method) {
    // TODO: implement onEndRequest
    Logger.root.info("onEndRequest $url $method");
  }

  @override
  void onErrorRequest(String url, String method) {
    // TODO: implement onErrorRequest
    Logger.root.info("onErrorRequest $url $method");
  }

  @override
  void onStartRequest(String url, String method) {
    // TODO: implement onStartRequest
    Logger.root.info("onStartRequest $url $method");
  }

  @override
  void onSuccessRequest(String url, String method) {
    // TODO: implement onSuccessRequest
    Logger.root.info("onSuccessRequest $url $method");
  }
}
