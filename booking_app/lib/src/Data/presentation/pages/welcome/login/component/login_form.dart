import 'package:booking_app/src/Data/presentation/pages/welcome/login/login_cubit.dart';
import 'package:booking_app/src/Data/presentation/widgets/app_input_text.dart';
import 'package:booking_app/src/Data/presentation/widgets/primary_button.dart';
import 'package:booking_app/src/Data/utils/extension/double_extension.dart';
import 'package:flutter/material.dart';
import 'package:booking_app/src/Data/config/theme/theme.dart';
import 'package:booking_app/src/Data/config/theme/appdimension.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LoginForm extends StatelessWidget {
  const LoginForm({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final cubit = BlocProvider.of<LoginCubit>(context);
    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: AppDimension.w24,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Login',
            style: AppFont.h3,
          ),
          16.0.height,
          AppInputText(
            label: 'Email',
            controller: cubit.state.emailController,
          ),
          16.0.height,
          AppInputText(
            label: 'Password',
            controller: cubit.state.passwordController,
          ),
          16.0.height,
          PrimaryButton(
            onPressed: cubit.login,
            text: 'Login',
            width: double.infinity,
            type: PrimaryButtonType.type3,
          ),
        ],
      ),
    );
  }
}
