import 'package:booking_app/src/Data/config/theme/theme.dart';
import 'package:booking_app/src/Data/utils/extension/double_extension.dart';
import 'package:flutter/material.dart';

class LoginFooter extends StatelessWidget {
  const LoginFooter({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        // 8.0.height,
        RichText(
            text: TextSpan(
              children: [
                TextSpan(
                  text: "Forgot Password",
                  style: AppFont.paragraphSmall.copyWith(color: AppColor.ink02),
                ),
                TextSpan(
                  text: " Forgot",
                  style: AppFont.paragraphSmallBold.copyWith(color: AppColor.ink01),
                ),
              ]
            ),
        ),
        8.0.height,
        RichText(
          text: TextSpan(
              children: [
                TextSpan(
                  text: "Don't have an account?",
                  style: AppFont.paragraphSmall.copyWith(color: AppColor.ink02),
                ),
                TextSpan(
                  text: " Create New",
                  style: AppFont.paragraphSmallBold.copyWith(color: AppColor.ink01),
                ),
              ]
          ),
        ),
      ],
    );
  }
}
