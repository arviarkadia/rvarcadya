import 'package:booking_app/src/Data/config/config.dart';
import 'package:booking_app/src/Data/presentation/pages/onboard/view.dart';
import 'package:booking_app/src/Data/presentation/pages/welcome/login/login_view.dart';
import 'package:booking_app/src/Data/presentation/widgets/primary_button.dart';
import 'package:booking_app/src/Data/presentation/widgets/secondary_button.dart';
import 'package:booking_app/src/Data/utils/extension/double_extension.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class WelcomeButton extends StatelessWidget {
  const WelcomeButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: AppDimension.w16),
      child: Row(
        children: [
          Expanded(
              child: PrimaryButton(
            onPressed: () {
              Get.to(OnboardPage());
            },
            text: 'Explore',
          )),
          16.0.width,
          Expanded(
              child: SecondaryButton(
                  onPressed: () {
                    Get.to(LoginPage());
                  },
                  text: 'Login')),
        ],
      ),
    );
  }
}
