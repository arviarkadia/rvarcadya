import 'package:booking_app/src/Data/config/config.dart';
import 'package:booking_app/src/Data/utils/extension/double_extension.dart';
import 'package:flutter/cupertino.dart';
import 'package:booking_app/src/Data/config/theme/appdimension.dart';
import 'package:booking_app/src/Data/config/config.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter/material.dart';

class WelcomeTitle extends StatelessWidget {
  const WelcomeTitle({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(AppDimension.w16),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          24.0.height,
          Text('Find the Perfect Roommate', style: AppFont.h3,),
          Text("We've helped millions across the nation find their perfect match... and you're next!", style: AppFont.paragraphMedium,),
        ],
      ),
    );
  }
}
