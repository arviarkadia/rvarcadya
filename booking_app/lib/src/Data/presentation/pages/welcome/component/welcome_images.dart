import 'package:booking_app/src/Data/data/src/img_string.dart';
import 'package:flutter/cupertino.dart';

class WelcomeImages extends StatelessWidget {
  const WelcomeImages({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: Image.asset(
      ImgString.giantPhone,
      fit: BoxFit.contain,
    ));
  }
}
