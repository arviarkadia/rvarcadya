import 'package:booking_app/src/Data/presentation/pages/discover/favorite/favorite_view.dart';
import 'package:booking_app/src/Data/presentation/pages/discover/setting/setting_view.dart';
import 'package:booking_app/src/Data/presentation/pages/discover/trips/trips_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../config/config.dart';
import 'component/discover_bottom_menu.dart';
import 'discover_cubit.dart';
import 'discover_state.dart';

class DiscoverPage extends StatelessWidget {
  const DiscoverPage({super.key});
  @override
  Widget build(BuildContext context) {
    // final ApiClient apiClient = ApiClient();
    // apiClient.get(Uri.parse(
    //     "https://43930851-c586-489a-8d60-49e5d5967078.mock.pstmn.io/api/v1/trips"));
    return BlocProvider(
      create: (BuildContext context) => DiscoverCubit(),
      child: Builder(builder: (context) => _buildPage(context)),
    );
  }

  Widget _buildPage(BuildContext context) {
    // final cubit = BlocProvider.of<DiscoverCubit>(context);
    return Scaffold(
      backgroundColor: AppColor.ink03,
      body: Column(
        children: [
          Expanded(child: BlocBuilder<DiscoverCubit, DiscoverState>(
            builder: (context, state) {
              return state.selectedIndex == 0
                  ? const TripsPage()
                  : state.selectedIndex == 1
                      ? const FavoritePage()
                      : const SettingPage();
            },
          )),
          BlocBuilder<DiscoverCubit, DiscoverState>(
            builder: (context, state) {
              // return BottomMenu(cubit: cubit,);
              return BottomMenu();
            },
          ),
        ],
      ),
    );
  }
}
