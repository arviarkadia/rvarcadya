import 'package:booking_app/src/Data/config/config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class FavoriteList extends StatelessWidget {
  const FavoriteList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: ListView.builder(
        itemBuilder: (c, i) => _listItem(),
        itemCount: 10,
      ),
    );
  }

  Widget _listItem() {
    return Container(
      margin: EdgeInsets.symmetric(
        horizontal: AppDimension.w16,
      ),
      // height: 76.0.h,
      child: Card(
        child: Padding(
          padding: EdgeInsets.all(AppDimension.w16),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Ngoding Malam", style: AppFont.paragraphMediumBold),
                  Text(
                    "Jakarta Indonesia",
                    style: AppFont.paragraphSmall.copyWith(
                      color: AppColor.ink02,
                    ),
                  ),
                ],
              ),
              Icon(Icons.chevron_right, color: AppColor.ink01, size: 32.0.sp,),
            ],
          ),
        ),
      ),
    );
  }
}
