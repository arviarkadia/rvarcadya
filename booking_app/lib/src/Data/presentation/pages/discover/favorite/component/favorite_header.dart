import 'package:booking_app/src/Data/config/config.dart';
import 'package:booking_app/src/Data/data/src/img_string.dart';
import 'package:booking_app/src/Data/utils/extension/double_extension.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class FavoriteHeader extends StatelessWidget {
  const FavoriteHeader({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 163.h,
      margin: EdgeInsets.all(AppDimension.w16),
      padding: EdgeInsets.all(AppDimension.w8),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(AppDimension.w8),
      ),
      child: Row(
        children: [
          Image.asset(ImgString.plants2),
          24.0.width,
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Italy",
                  style: AppFont.paragraphSmall,
                ),
                8.0.height,
                Text(
                  "Meet new roomies and find new adventures.",
                  style: AppFont.h4.copyWith(
                    height: 1.2.sp,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
