import 'package:booking_app/src/Data/data/models/trip/trip.dart';
import 'package:booking_app/src/Data/data/src/img_string.dart';
import 'package:booking_app/src/Data/presentation/pages/discover/trips/trips_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:logging/logging.dart';

import '../../../../../config/config.dart';
import '../trips_cubit.dart';

class TripsYourRoomies extends StatelessWidget {
  const TripsYourRoomies({Key? key, required this.yourRoomies})
      : super(key: key);
  final List<Trip> yourRoomies;

  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: Column(
      children: [
        _header(),
        _roomiesList(context),
      ],
    ));
  }

  Widget _roomiesList(BuildContext context) {
    // final cubit = BlocProvider.of<TripsCubit>(context);
    return Expanded(
      child: ListView.builder(
        itemBuilder: (c, i) => _roomiesItem(/*yourRoomies[i]*/),
        itemCount: 3,
      ),
    );
  }

  Widget _roomiesItem(/*Trip trip*/) {
    return Container(
      padding: EdgeInsets.symmetric(
          horizontal: AppDimension.w16, vertical: AppDimension.h8),
      margin: EdgeInsets.only(
        bottom: AppDimension.h16,
        left: AppDimension.w16,
        right: AppDimension.w16,
      ),
      height: 72,
      width: double.infinity,
      decoration: BoxDecoration(
        color: AppColor.ink05,
        borderRadius: BorderRadius.circular(AppDimension.w8),
      ),
      child: Row(
        children: [
          _avatar(),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  "Jono",
                  style: AppFont.paragraphMediumBold,
                ),
                Text(
                  "Wakanda",
                  style: AppFont.paragraphSmall.copyWith(color: AppColor.ink02),
                ),
              ],
            ),
          ),
          const Icon(Icons.more_vert),
        ],
      ),
    );
  }

  Widget _avatar() {
    return SizedBox(
      height: 56.0.w,
      width: 56.0.w,
      child: Stack(
        children: [
          Center(
            child: CircleAvatar(
              radius: 28.0.w,
              backgroundColor: AppColor.ink03,
            ),
          ),
          Center(
            child: CircleAvatar(
              radius: 24.0.w,
              backgroundColor: AppColor.ink05,
              backgroundImage: const AssetImage(ImgString.avatar1),
            ),
          ),
        ],
      ),
    );
  }

  Widget _header() {
    return Padding(
      padding: EdgeInsets.only(
        left: AppDimension.h16,
        right: AppDimension.h16,
        top: AppDimension.h60,
      ),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: [
          Text(
            "Your Roomies",
            style: AppFont.h3,
          ),
          const Expanded(child: SizedBox()),
          IconButton(
              onPressed: () {},
              icon: const Icon(
                Icons.search,
                color: AppColor.ink02,
              )),
          IconButton(
              onPressed: () {},
              icon: const Icon(
                Icons.star,
                color: AppColor.ink02,
              )),
        ],
      ),
    );
  }
}
