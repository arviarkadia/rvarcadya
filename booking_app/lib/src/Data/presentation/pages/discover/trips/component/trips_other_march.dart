import 'package:booking_app/src/Data/utils/extension/double_extension.dart';
import 'package:flutter/material.dart';

import '../../../../../config/config.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:booking_app/src/Data/data/src/img_string.dart';

class TripsOtherMatches extends StatelessWidget {
  const TripsOtherMatches({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _header(),
          _roomiesList(context),
        ],
      ),
    );
  }

  Widget _roomiesList(BuildContext context) {
    return Expanded(
      child: ListView.builder(
        itemBuilder: (c, i) => _roomiesItem(context),
        itemCount: 3,
        scrollDirection: Axis.horizontal,
      ),
    );
  }

  Widget _roomiesItem(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(
        right: AppDimension.w16,
        left: AppDimension.w16,
        bottom: AppDimension.h16,
        top: 49.h,
      ),
      margin: EdgeInsets.only(
        top: AppDimension.h24,
        bottom: AppDimension.h16,
        left: AppDimension.w16,
      ),
      // height: 72,
      width: (MediaQuery.of(context).size.width / 2) - AppDimension.w38,
      decoration: BoxDecoration(
        color: AppColor.ink05,
        borderRadius: BorderRadius.circular(AppDimension.w16),
      ),
      child: Column(
        children: [
          Expanded(child: Image.asset(ImgString.plants)),
          37.0.height,
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "Bono",
                style: AppFont.paragraphLargeBold,
              ),
              Text(
                "\$400 USD",
                style: AppFont.paragraphSmall,
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _avatar() {
    return SizedBox(
      height: 56.0.w,
      width: 56.0.w,
      child: Stack(
        children: [
          Center(
            child: CircleAvatar(
              radius: 28.0.w,
              backgroundColor: AppColor.ink03,
            ),
          ),
          Center(
            child: CircleAvatar(
              radius: 24.0.w,
              backgroundColor: AppColor.ink05,
              backgroundImage: const AssetImage(ImgString.avatar1),
            ),
          ),
        ],
      ),
    );
  }

  Widget _header() {
    return Padding(
      padding: EdgeInsets.only(
        left: AppDimension.h16,
        right: AppDimension.h16,
        top: AppDimension.h24,
      ),
      child: Text(
        "Other Matches",
        style: AppFont.h3,
      ),
    );
  }
}
