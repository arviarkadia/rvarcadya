import 'package:bloc/bloc.dart';
import 'package:booking_app/src/Data/config/config.dart';
import 'package:booking_app/src/Data/domain/controller/trip_controller.dart';
import 'package:logging/logging.dart';

import 'trips_state.dart';

class TripsCubit extends Cubit<TripsState> implements HttpState {
  TripsCubit() : super(TripsState().init());

  late final TripController _tripController = TripController(this);

  void getTrips() async {
    final trips = await _tripController.getTrips();
    Logger.root.info(trips.result?.toJson());
    emit(
      state.clone()
        ..yourRoomies = trips.result?.yourRoomies ?? []
        ..otherMatches = trips.result?.otherMatches ?? [],
    );
    Logger.root.info(
        "TripsCubit getTrips ${state.yourRoomies.length} ${state.yourRoomies.length}");
  }

  @override
  void onEndRequest(String url, String method) {
    // TODO: implement onEndRequest
  }

  @override
  void onErrorRequest(String url, String method) {
    // TODO: implement onErrorRequest
  }

  @override
  void onStartRequest(String url, String method) {
    // TODO: implement onStartRequest
  }

  @override
  void onSuccessRequest(String url, String method) {
    // TODO: implement onSuccessRequest
  }
}
