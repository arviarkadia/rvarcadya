import 'package:booking_app/src/Data/config/theme/theme.dart';
import 'package:booking_app/src/Data/utils/extension/double_extension.dart';
import 'package:flutter/material.dart';

// import 'package:booking_app/src/Data/';
class AppInputText extends StatefulWidget {
  const AppInputText({
    Key? key,
    this.controller,
    required this.label,
  }) : super(key: key);
  final TextEditingController? controller;
  final String label;

  @override
  State<AppInputText> createState() => _AppInputTextState();
}

class _AppInputTextState extends State<AppInputText> {
  // _AppInputTextState({required this.label, required this.controller,})
  // final TextEditingController controller;
  // final String label;
  @override
  void initState() {
    // if(widget.controller == null){
    //   controller = TextEditingController();
    // }
    // else{
    //   controller = widget.controller!;
    // }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: AppDimension.h40,
      child: TextFormField(
        controller: widget.controller,
        decoration: InputDecoration(
          hintText: widget.label,
          hintStyle: AppFont.commponentMedium,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(4),
          ),
          contentPadding: EdgeInsets.symmetric(horizontal: AppDimension.w16),
        ),
      ),
    );
  }
}
