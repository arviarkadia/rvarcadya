import 'dart:convert';

import 'package:booking_app/src/Data/config/config.dart';
import 'package:booking_app/src/Data/config/networking/base_response.dart';
import 'package:booking_app/src/Data/data/endpoint/endpoint.dart';
import 'package:booking_app/src/Data/domain/repositories/trip_repository.dart';

class TripController implements TripRepository {
  final ApiClient _client = ApiClient();
  final HttpState httpState;
  TripController(this.httpState);
  @override
  Future<BaseResponse> detailTrips(int id) async {
    String url = "${Endpoint.trips}/$id";
    String method = "GET";
    httpState.onStartRequest(url, method);

    late final BaseResponse baseResponse;
    final response = await _client.get(
      Uri.parse(url),
    );

    httpState.onEndRequest(url, method);

    if (response.statusCode < 500) {
      if (response.statusCode > 199 && response.statusCode < 300) {
        httpState.onSuccessRequest(url, method);
        // baseResponse = BaseResponse.fromJson(json.decode(response.body));
      } else {
        httpState.onErrorRequest(url, method);
        // baseResponse = BaseResponse.fromJson(json.decode(response.body));
      }
      baseResponse = BaseResponse.fromJson(jsonDecode(response.body));
    } else {
      baseResponse = BaseResponse(message: response.body);
    }
    return baseResponse;
  }

  @override
  Future<BaseResponse> getTrips() async {
    String url = Endpoint.trips;
    String method = "GET";
    httpState.onStartRequest(url, method);

    late final BaseResponse baseResponse;
    final response = await _client.get(
      Uri.parse(url),
    );

    httpState.onEndRequest(url, method);

    if (response.statusCode < 500) {
      if (response.statusCode > 199 && response.statusCode < 300) {
        httpState.onSuccessRequest(url, method);
        // baseResponse = BaseResponse.fromJson(json.decode(response.body));
      } else {
        httpState.onErrorRequest(url, method);
        // baseResponse = BaseResponse.fromJson(json.decode(response.body));
      }
      baseResponse = BaseResponse.fromJson(jsonDecode(response.body));
    } else {
      baseResponse = BaseResponse(message: response.body);
    }
    return baseResponse;
  }
}
