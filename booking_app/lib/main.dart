import 'package:booking_app/src/Data/data/src/img_string.dart';
import 'package:booking_app/src/Data/presentation/pages/discover/discover_view.dart';
import 'package:booking_app/src/Data/presentation/pages/discover/favorite/favorite_view.dart';
import 'package:booking_app/src/Data/presentation/pages/onboard/cubit.dart';
import 'package:booking_app/src/Data/presentation/pages/onboard/view.dart';
import 'package:booking_app/src/Data/presentation/pages/welcome/login/login_view.dart';
import 'package:booking_app/src/Data/presentation/pages/welcome/welcome_done/welcome_done_view.dart';
import 'package:booking_app/src/Data/presentation/pages/welcome/welcome_view.dart';
import 'package:booking_app/src/Data/presentation/widgets/primary_button.dart';
import 'package:booking_app/src/Data/presentation/widgets/secondary_button.dart';
import 'package:booking_app/src/Data/utils/helpers/pref_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:logging/logging.dart';

// import 'src/Data/presentation/pages/onboard/view.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await PrefHelper.instance.init();
  Logger.root.level = Level.ALL;
  Logger.root.onRecord.listen((record) {
    debugPrint(record.message);
  });
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
        designSize: const Size(375, 812),
        minTextAdapt: true,
        splitScreenMode: true,
        builder: (context, child) {
          return GetMaterialApp(
            title: 'Flutter Demo',
            theme: ThemeData(
              primarySwatch: Colors.blue,
              fontFamily: 'Poppins',
            ),
            // home: const OnboardingPage(),
            // home: const OnboardPage(),
            // home: const WelcomePage(),
            // home: const LoginPage(),
            // home: const WelcomedonePage(),
            // home: const DiscoverPage(),
            // home: const FavoritePage(),
            home: const WelcomePage(),
          );
        });
  }
}
