import 'package:flutter/material.dart';
import 'package:mini_mediation_app/widgets/bottom_nav_item.dart';

class BottomNavBar extends StatelessWidget {
  const BottomNavBar({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      height: 80,
      color: Colors.white,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          // Icon Item
          BottomNav(
            svgSrc: 'assets/icons/calendar.svg',
            text: 'Date',
            press: () {},
          ),
          BottomNav(
            svgSrc: 'assets/icons/gym.svg',
            text: 'All Exercises',
            press: () {},
            isActive: true,
          ),
          BottomNav(
            svgSrc: 'assets/icons/Settings.svg',
            text: 'Settings',
            press: () {},
          ),
        ],
      ),
    );
  }
}
