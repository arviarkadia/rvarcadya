import 'package:basic_slicingui/chat_page.dart';
import 'package:basic_slicingui/home_page.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Slicing UI',
      theme: ThemeData(
        fontFamily: 'Poppins',
      ),
      home: ChatPage(),
    );
  }
}
