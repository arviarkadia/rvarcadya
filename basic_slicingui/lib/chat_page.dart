import 'package:flutter/material.dart';
import 'package:basic_slicingui/theme.dart';

class ChatPage extends StatelessWidget {
  const ChatPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: blueColor,
      body: SingleChildScrollView(
        child: SafeArea(
          // header
          child: Center(
            child: Column(
              children: [
                SizedBox(height: 40),
                Image.asset(
                  "assets/icons/Asset5A.png",
                  width: 100,
                  height: 100,
                ),
                SizedBox(height: 20),
                Text(
                  "Arvi Arkadia",
                  style: TextStyle(
                    fontSize: 20,
                    color: whiteColor,
                  ),
                ),
                SizedBox(height: 2),
                Text(
                  "Flutter Developer",
                  style: TextStyle(
                    fontSize: 16,
                    color: lightBlueColor,
                  ),
                ),
                SizedBox(height: 30),
                Container(
                  width: double.infinity,
                  padding: EdgeInsets.all(30),
                  decoration: BoxDecoration(
                    color: whiteColor,
                    borderRadius: BorderRadius.vertical(
                      top: Radius.circular(40),
                    ),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Friends",
                        style: titleText,
                      ),
                      SizedBox(height: 16),
                      // item friends
                      Row(
                        children: [
                          Image.asset(
                            "assets/icons/Asset1A.png",
                            width: 50,
                            height: 50,
                          ),
                          SizedBox(width: 12),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Joshuea amanda",
                                style: titleText,
                              ),
                              Text(
                                "Hello hais",
                                style: subTitleText,
                              ),
                            ],
                          ),
                          Spacer(),
                          Text(
                            "Now",
                            style: subTitleText.copyWith(color: blackColor),
                          ),
                        ],
                      ),
                      // item friends
                      SizedBox(height: 15),
                      // item friends
                      Row(
                        children: [
                          Image.asset(
                            "assets/icons/Asset2A.png",
                            width: 50,
                            height: 50,
                          ),
                          SizedBox(width: 12),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "amanda Lolyd",
                                style: titleText,
                              ),
                              Text(
                                "Hello hais",
                                style: subTitleText,
                              ),
                            ],
                          ),
                          Spacer(),
                          Text(
                            "2:30",
                            style: subTitleText.copyWith(color: blackColor),
                          ),
                        ],
                      ),
                      // item friends
                      SizedBox(height: 20),
                      Text(
                        "Groups",
                        style: titleText,
                      ),
                      SizedBox(height: 16),
                      // item friends
                      Row(
                        children: [
                          Image.asset(
                            "assets/icons/Asset1A.png",
                            width: 50,
                            height: 50,
                          ),
                          SizedBox(width: 12),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Joshuea amanda",
                                style: titleText,
                              ),
                              Text(
                                "Hello hais",
                                style: subTitleText,
                              ),
                            ],
                          ),
                          Spacer(),
                          Text(
                            "Now",
                            style: subTitleText.copyWith(color: blackColor),
                          ),
                        ],
                      ),
                      // item friends
                      SizedBox(height: 15),
                      // item friends
                      Row(
                        children: [
                          Image.asset(
                            "assets/icons/Asset2A.png",
                            width: 50,
                            height: 50,
                          ),
                          SizedBox(width: 12),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "amanda Lolyd",
                                style: titleText,
                              ),
                              Text(
                                "Hello hais",
                                style: subTitleText,
                              ),
                            ],
                          ),
                          Spacer(),
                          Text(
                            "2:30",
                            style: subTitleText.copyWith(color: blackColor),
                          ),
                        ],
                      ),
                      // item friends
                      SizedBox(height: 20),
                      Center(
                        child: FloatingActionButton(
                          onPressed: () {},
                          child: Icon(
                            Icons.add,
                            color: whiteColor,
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
          // header
        ),
      ),
    );
  }
}
