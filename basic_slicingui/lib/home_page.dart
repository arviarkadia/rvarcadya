import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              // image
              Image.asset(
                "assets/business.png",
                width: 500,
                height: 300,
              ),
              // item
              SizedBox(height: 30),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 30),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    // title
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Pantai Teluk Penyu",
                          style: TextStyle(fontWeight: FontWeight.w700),
                        ),
                        SizedBox(height: 8),
                        Text(
                          "Cilacap Jawa Tengah",
                          style: TextStyle(
                            color: Colors.black.withOpacity(.5),
                            fontSize: 12,
                          ),
                        ),
                      ],
                    ),
                    // icon
                    Row(
                      children: [
                        Icon(
                          Icons.favorite,
                          color: Colors.orange,
                        ),
                        Text(
                          "4.5",
                          style: TextStyle(
                            fontSize: 20,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              // item
              // items icon
              SizedBox(height: 30),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 30),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.phone,
                          size: 20,
                          color: Colors.blue,
                        ),
                        Text(
                          "Call",
                          style: TextStyle(
                            color: Colors.blue,
                            fontSize: 12,
                          ),
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.navigation,
                          size: 20,
                          color: Colors.blue,
                        ),
                        Text(
                          "Route",
                          style: TextStyle(
                            color: Colors.blue,
                            fontSize: 12,
                          ),
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.share,
                          size: 20,
                          color: Colors.blue,
                        ),
                        Text(
                          "Share",
                          style: TextStyle(
                            color: Colors.blue,
                            fontSize: 12,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              // items icon
              // text
              SizedBox(height: 30),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 30),
                child: Text(
                  "Pada umumnya, RPA Developer  adalah seseorang yang bekerja secara lintas fungsi dengan operasi bisnis dan analisis bisnis untuk merancang dan mengelola proyek otomatisasi alur kerja. RPA Developer bertanggung jawab dalam  merancang, mengembangkan, dan mengimplementasikan sistem RPA. Sistem RPA mengacu pada teknologi otomatisasi canggih yang dirancang untuk meningkatkan produktivitas bisnis dan memaksimalkan efisiensi. Sederhananya, RPA developer menciptakan robot software atau bot untuk bekerjasama dengan manusia, guna meningkatkan efisiensi proses bisnis, atau alur kerja.",
                  style: TextStyle(
                    fontSize: 12,
                  ),
                ),
              ),
              // text
            ],
          ),
        ),
      ),
    );
  }
}
