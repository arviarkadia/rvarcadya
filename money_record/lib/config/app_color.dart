import 'package:flutter/cupertino.dart';

class AppColor {
  static const primary = Color(0xff767ae7);
  static const secondary = Color(0xffb9fffc);
  static const bgColor1 = Color.fromARGB(255, 186, 201, 240);
  static const bgColor2 = Color(0xffa3d8f4);
}
